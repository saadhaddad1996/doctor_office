from django.db import models
from account.models import User
from core.models import TimeStampedModel

class Doctor(TimeStampedModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    specialization = models.CharField(max_length=255,blank=True, null=True)