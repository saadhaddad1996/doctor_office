import json
import os
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from doctor.utils import create_doctor

User = get_user_model()

class Command(BaseCommand):
    help = 'Populates the database with doctors from a JSON file'

    def handle(self, *args, **kwargs):
        # Get the path to the JSON file within the commands directory
        file_path = os.path.join(os.path.dirname(__file__), 'doctors.json')
        
        try:
            with open(file_path, 'r') as file:
                doctors_data = json.load(file)
        except FileNotFoundError:
            self.stdout.write(self.style.ERROR('File not found'))
            return
        except json.JSONDecodeError:
            self.stdout.write(self.style.ERROR('Invalid JSON file'))
            return

        for doctor_data in doctors_data:
            email = doctor_data['email']
            name = doctor_data['name']
            password = doctor_data['password']
            specialization = doctor_data['specialization']

            if not User.objects.filter(email=email).exists():
                doctor =create_doctor(email=email, name=name, password=password, specialization=specialization)
                self.stdout.write(self.style.SUCCESS(f'Successfully created doctor: {doctor.user.email}'))
            else:
                self.stdout.write(self.style.WARNING(f'Doctor with email {email} already exists'))
