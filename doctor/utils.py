from account.models import User
from .models import Doctor



def create_doctor(email:str,name:str,password:str,specialization:str):
    extra_fields={
        'is_staff':True,
    }
    user = User.objects.create_user(email,name,password,**extra_fields)
    doctor = Doctor.objects.create(user=user,specialization=specialization)
    return doctor


