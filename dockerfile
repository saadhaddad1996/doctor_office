# Use an official Python runtime with Alpine Linux as a parent image
FROM python:3.9-alpine

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV DEBUG=1
# Set the working directory in the container
WORKDIR /app

# Update package lists and install necessary packages
RUN apk add --no-cache \
    git \
    gcc \
    python3-dev \
    libxml2-dev \
    libxslt-dev \
    build-base \
    py3-lxml \
    zlib-dev \
    mariadb-connector-c-dev \
    wget

# Copy the current directory contents into the container at /app
COPY . /app

# Install any needed packages specified in requirements.txt
RUN pip install --no-cache-dir -r requirements.txt


RUN chmod +x /app/populate_db.sh
# Expose port 8000 for the Django application
EXPOSE 8000

# Run Django's development server
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
