from rest_framework import serializers
from .models import Appointment
from datetime import datetime
from doctor.serializers import DoctorSerializer
from patient.serializers import PatientSerializer

class AppointmentSerializer(serializers.ModelSerializer):
    doctor = DoctorSerializer(read_only=True)
    patient = PatientSerializer(read_only=True)

    class Meta:
        model = Appointment
        fields = '__all__'




class AppointmentCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appointment
        fields =  ['doctor', 'patient', 'date', 'time','reason']

    def validate(self, data):
        # Check for past date
        if data['date'] < datetime.today().date():
            raise serializers.ValidationError("Appointment date cannot be in the past.")
        
        # Check for overlapping appointments for the same doctor
        doctor = data['doctor']
        date = data['date']
        time = data['time']
        patient = data['patient']
        
        overlapping_appointments = Appointment.objects.filter(
            doctor=doctor, 
            date=date, 
            time=time
        ).exclude(id=self.instance.id if self.instance else None)
        
        if overlapping_appointments.exists():
            raise serializers.ValidationError("This doctor already has an appointment at the selected time.")
        
        # Check if another doctor has an appointment with the same patient at the same time
        conflicting_appointments = Appointment.objects.filter(
            patient=patient,
            date=date,
            time=time
        ).exclude(id=self.instance.id if self.instance else None)

        if conflicting_appointments.exists():
            raise serializers.ValidationError("Another doctor has an appointment with the same patient at the selected time.")
        
        return data



class AppointmentUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appointment
        fields =  ['doctor', 'patient', 'date', 'time','reason']

    def validate(self, data):
        # Check for past date
        if data['date'] < datetime.today().date():
            raise serializers.ValidationError("Appointment date cannot be in the past.")
        
        # Check for overlapping appointments for the same doctor
        doctor = data['doctor']
        date = data['date']
        time = data['time']
        patient = data['patient']
        
        overlapping_appointments = Appointment.objects.filter(
            doctor=doctor, 
            date=date, 
            time=time
        ).exclude(id=self.instance.id if self.instance else None)
        
        if overlapping_appointments.exists():
            raise serializers.ValidationError("This doctor already has an appointment at the selected time.")
        
        # Check if another doctor has an appointment with the same patient at the same time
        conflicting_appointments = Appointment.objects.filter(
            patient=patient,
            date=date,
            time=time
        ).exclude(id=self.instance.id if self.instance else None)

        if conflicting_appointments.exists():
            raise serializers.ValidationError("Another doctor has an appointment with the same patient at the selected time.")
        
        return data