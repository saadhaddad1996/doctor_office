from django.db import models
from patient.models import Patient
from doctor.models import Doctor
from core.models import TimeStampedModel


class Appointment(TimeStampedModel):
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE,related_name='appointments')
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE,related_name='appointments')
    date = models.DateField()
    time = models.TimeField()
    reason = models.TextField(blank=True, null=True)