

from django.urls import path
from .views import(
       AppointmentListView,
       AppointmentCreateView,
       AppointmentCancelView,
       AppointmentUpdateView,
       AppointmentRetrieveView,
)

urlpatterns = [
    path('', AppointmentListView.as_view(), name='appointment-list'),
    path('<int:pk>/', AppointmentRetrieveView.as_view(), name='appointment-retrive'),
    path('create/', AppointmentCreateView.as_view(), name='appointment-create'),
    path('<int:pk>/update/', AppointmentUpdateView.as_view(), name='appointment-update'),
    path('<int:pk>/cancel/', AppointmentCancelView.as_view(), name='appointment-cancel'),
]