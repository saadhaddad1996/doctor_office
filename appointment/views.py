from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from drf_spectacular.utils import OpenApiResponse
from account.permissions import IsDoctor, IsPatient, IsAdminStaff
from .models import Appointment,Patient
from .serializers import AppointmentCreateSerializer,AppointmentUpdateSerializer,AppointmentSerializer
from drf_spectacular.utils import extend_schema
from rest_framework.exceptions import NotFound, PermissionDenied

class AppointmentListView(APIView):
    permission_classes = [IsDoctor | IsPatient | IsAdminStaff]
    @extend_schema(
        summary="List appointments",
        description="Retrieves a list of appointments. The returned appointments are filtered based on the user's role. "
                    "Doctors and admin staff see all appointments, while patients see only their appointments.",
        responses={200: AppointmentSerializer(many=True)},
        tags=['Appointments']
    )
    def get(self, request):
        user = request.user
        appointments =Appointment.objects.all()

        if IsPatient().has_permission(request, self):
            appointments =appointments.filter(patient=user.patient)
    
        serializer = AppointmentSerializer(appointments, many=True)
        return Response(serializer.data)
    

class AppointmentRetrieveView(APIView):
    permission_classes = [IsDoctor | IsPatient | IsAdminStaff]
    
    @extend_schema(
        summary="Retrieve an appointment",
        description="Fetches a detailed view of an appointment by its ID. Access is granted based on the user's role relative to the appointment.",
        responses={
            200: AppointmentSerializer,
            403: OpenApiResponse(description="Unauthorized to view this appointment."),
            404: OpenApiResponse(description="Appointment not found.")
        },
        tags=['Appointments']
    )
    def get(self, request, pk):
        try:
            appointment = Appointment.objects.get(pk=pk)
        except Appointment.DoesNotExist:
            raise NotFound("Appointment not found.")
        
    
        self.check_object_permissions(request, appointment)
        
        serializer = AppointmentSerializer(appointment)
        return Response(serializer.data)


class AppointmentCreateView(APIView):
    permission_classes = [ IsDoctor]
    @extend_schema(
        summary="Create a new appointment",
        description="Allows a doctor to create a new appointment. Requires patient ID to be specified in the request.",
        request=AppointmentCreateSerializer,
        responses={
            201: AppointmentSerializer,
            400: OpenApiResponse(description="Bad request due to missing patient ID or validation errors."),
            404: OpenApiResponse(description="Patient not found.")
        },
         tags=['Appointments']
    )
    def post(self, request):
        user = request.user
        data = request.data.copy()
        data['doctor'] = user.doctor.id

        # Ensure patient ID is provided in the request data
        patient_id = data.get('patient')
        if not patient_id:
            return Response({"detail": "Patient ID is required."}, status=status.HTTP_400_BAD_REQUEST)
        
        try:
            _ = Patient.objects.get(id=patient_id)
        except Patient.DoesNotExist:
              raise NotFound("Patient not found.")

        
        serializer = AppointmentCreateSerializer(data=data)
        if serializer.is_valid():
            appointment =serializer.save()
            response_serializer = AppointmentSerializer(appointment)
            return Response(response_serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    

class AppointmentUpdateView(APIView):
    permission_classes = [IsDoctor]

    def get_object(self, pk, request):
        """
        Helper method to get the object and handle permission checking.
        """
        try:
            appointment = Appointment.objects.get(pk=pk)
        except Appointment.DoesNotExist:
            raise NotFound("Appointment not found.")

        if appointment.doctor != request.user.doctor:
            raise PermissionDenied("Unauthorized to uddate this appointment.")

        return appointment
    
    @extend_schema(
    summary="Update an appointment",
    description="Updates specific details of an appointment if the requesting user is the assigned doctor.",
    request=AppointmentUpdateSerializer,
    responses={
        200: AppointmentSerializer,
        400: OpenApiResponse(description="Bad request due to validation failure."),
        403: OpenApiResponse(description="Unauthorized to update this appointment."),
        404: OpenApiResponse(description="Appointment not found.")
    },
    tags=['Appointments']
    )
    def put(self, request, pk):
        appointment = self.get_object(pk,request)
     
        user = request.user
        data = request.data.copy()
        data['doctor'] = user.doctor.id

        patient_id = data.get('patient')
        if not patient_id:
            return Response({"detail": "Patient ID is required."}, status=status.HTTP_400_BAD_REQUEST)
        
        try:
            _ = Patient.objects.get(id=patient_id)
        except Patient.DoesNotExist:
              raise NotFound("Patient not found.")


        serializer = AppointmentUpdateSerializer(appointment, data=data, partial=True)
        if serializer.is_valid():
            appointment = serializer.save()
            response_serializer = AppointmentSerializer(appointment)
            return Response(response_serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AppointmentCancelView(APIView):
    permission_classes = [IsDoctor]

    def get_object(self, pk, request):
        """
        Helper method to get the object and handle permission checking.
        """
        try:
            appointment = Appointment.objects.get(pk=pk)
        except Appointment.DoesNotExist:
            raise NotFound("Appointment not found.")

        if appointment.doctor != request.user.doctor:
            raise PermissionDenied("Unauthorized to cancel this appointment.")

        return appointment
    @extend_schema(
    summary="Cancel an appointment",
    description="Cancels a specific appointment if the requesting user is the assigned doctor.",
    responses={
        204: OpenApiResponse(response=None, description="No content, appointment successfully cancelled."),
        403: OpenApiResponse(response=None, description="Unauthorized to cancel this appointment."),
        404: OpenApiResponse(response=None, description="Appointment not found.")
    },
    tags=['Appointments']
)
    def delete(self, request, pk):
        appointment = self.get_object(pk,request)
        appointment.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)