from django.db import models
from account.models import User 
from core.models import TimeStampedModel

class Patient(TimeStampedModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    date_of_birth = models.DateField(blank=True, null=True)
    address = models.TextField(blank=True, null=True)