from account.models import User
from .models import Patient



def create_patient(email:str,name:str,password:str,date_of_birth:str=None,address:str=None):
    extra_fields={
        'is_staff':False,
    }
    user = User.objects.create_user(email,name,password,**extra_fields)
    patient = Patient.objects.create(user=user,date_of_birth=date_of_birth,address=address)
    return patient


