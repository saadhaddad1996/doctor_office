
from rest_framework import serializers
from .models import Patient


class PatientSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='user.name', read_only=True)
    email = serializers.EmailField(source='user.email', read_only=True)
    class Meta:
        model = Patient
        fields = ['id', 'name', 'email', 'date_of_birth', 'address'] 