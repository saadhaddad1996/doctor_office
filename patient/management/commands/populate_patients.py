import json
import os
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from patient.utils import create_patient

User = get_user_model()

class Command(BaseCommand):
    help = 'Populates the database with patients from a JSON file'

    def handle(self, *args, **kwargs):
        # Get the path to the JSON file within the commands directory
        file_path = os.path.join(os.path.dirname(__file__), 'patients.json')
        
        try:
            with open(file_path, 'r') as file:
                patients_data = json.load(file)
        except FileNotFoundError:
            self.stdout.write(self.style.ERROR('File not found'))
            return
        except json.JSONDecodeError:
            self.stdout.write(self.style.ERROR('Invalid JSON file'))
            return

        for patient_data in patients_data:
            email = patient_data['email']
            name = patient_data['name']
            password = patient_data['password']
            date_of_birth = patient_data['date_of_birth']
            address = patient_data['address']

            if not User.objects.filter(email=email).exists():
                patient =create_patient(email=email, name=name, password=password, date_of_birth=date_of_birth, address=address)
                self.stdout.write(self.style.SUCCESS(f'Successfully created patient: {patient.user.email}'))
            else:
                self.stdout.write(self.style.WARNING(f'Patient with email {email} already exists'))
