#!/bin/sh

# Run the Django management commands to populate the database
python manage.py populate_doctors
python manage.py populate_patients
python manage.py populate_administration_staff

# Print a message indicating completion
echo "Database population complete."
