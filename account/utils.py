from account.models import User




def create_administration_staff(email:str,name:str,password:str):
    extra_fields={
        'is_staff':True,
    }
    staff = User.objects.create_user(email,name,password,**extra_fields)
   
    return staff


