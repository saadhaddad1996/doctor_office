from rest_framework.permissions import BasePermission

class IsDoctor(BasePermission):
    """
    Allows access only to doctors.
    """
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_authenticated and hasattr(request.user, 'doctor'))

class IsAdminStaff(BasePermission):
    """
    Allows access only to administrative staff (users who are not doctors and not patients).
    """
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_authenticated and 
                    not hasattr(request.user, 'doctor') and 
                    not hasattr(request.user, 'patient'))

class IsPatient(BasePermission):
    """
    Allows access only to patients.
    """
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_authenticated and hasattr(request.user, 'patient'))

    def has_object_permission(self, request, view, obj):
        return obj.patient.user == request.user
