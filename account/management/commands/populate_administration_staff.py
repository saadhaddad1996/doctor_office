import json
import os
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from account.utils import create_administration_staff

User = get_user_model()

class Command(BaseCommand):
    help = 'Populates the database with administration staff from a JSON file'

    def handle(self, *args, **kwargs):
        # Get the path to the JSON file within the commands directory
        file_path = os.path.join(os.path.dirname(__file__), 'administration_staff.json')
        
        try:
            with open(file_path, 'r') as file:
                staff_data = json.load(file)
        except FileNotFoundError:
            self.stdout.write(self.style.ERROR('File not found'))
            return
        except json.JSONDecodeError:
            self.stdout.write(self.style.ERROR('Invalid JSON file'))
            return

        for staff in staff_data:
            email = staff['email']
            name = staff['name']
            password = staff['password']
            position = staff['position']

            if not User.objects.filter(email=email).exists():
                staff = create_administration_staff(email=email, name=name, password=password)
            
                self.stdout.write(self.style.SUCCESS(f'Successfully created staff: {staff.email}'))
            else:
                self.stdout.write(self.style.WARNING(f'Staff with email {email} already exists'))
